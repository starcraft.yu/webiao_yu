#define securify group
resource "tencentcloud_security_group" "sg01" {
  name        = "${var.name}-sg"
  description = "${var.name} security group @ powered by terraform"
}

resource "tencentcloud_security_group_lite_rule" "sg01-rule" {
  security_group_id = tencentcloud_security_group.sg01.id

  ingress = [
    "ACCEPT#0.0.0.0/0#ALL#ICMP",
    "ACCEPT#0.0.0.0/0#22#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#TCP",
    "ACCEPT#0.0.0.0/0#30000-32768#UDP",
  ]

  egress = [
    "ACCEPT#0.0.0.0/0#ALL#ALL",
  ]
}

