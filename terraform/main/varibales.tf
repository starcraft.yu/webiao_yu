#cluster name

variable "name" {
  default = "test-tke"
}

#TKE region

variable "region" {
  default = "ap-bangkok"
}

#TKE k8s verison
variable "k8s_ver" {
  default = "1.18.4"
}

#pod cidr
variable "pod_ip_seg" {
  default = "172.16"
}

#vpc cidr
variable "vpc_ip_seg" {
  default = "10.0"
}

#default type
variable "default_instance_type" {
  default = "S2.MEDIUM4"
}

#node password
variable "node_password" {
  default = "P*********"
}


