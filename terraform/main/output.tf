#kubeconfig
output "KUBECONFIG" {
  description = "print kubeconfig"
  value       = tencentcloud_kubernetes_cluster.tke_managed.kube_config
}
