# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/tencentcloudstack/tencentcloud" {
  version = "1.60.25"
  hashes = [
    "h1:3BteJUZ0zXVXhKffXHCjeQAMI20fTvjfrNGUSYPBzAU=",
    "zh:0dc2c54d670d03fbd296a95171342267a0f433dfdd463bda550a4124ea2de795",
    "zh:3f7123ab938a099d1dbf463f96537d6d850b1a3bb504686baad34fc2b3a773a9",
    "zh:621011e9c70f03f77da8ef1e53259b85e955d29c5c3064b0256b14a05bf84b3c",
    "zh:7975832f124f7bcd4241154f2531b5dc88057e8dde5f71514fb42989da8ac703",
    "zh:7c87f7c53c11ab5f2dcd8e45d344c1b476a4b029d76c3051b3d97e205b39ea3d",
    "zh:86e4f093ae0992807466eb4b5bef180c3a3c8416a40bebcbcf866d835ea324e4",
    "zh:a61de2d642f7316686094fc83e854e45e95bfaa5d5242857667febbf6b54b62f",
    "zh:b96c28ecf0f77ae3d9e04276c4e54421f1442ad208cb9d0ce432a5a65d93052c",
    "zh:c4ad1b0943154dc0b22ba1c4b61dd076f0c96b0419f31bfed166e46a54dae588",
    "zh:cbbe880d4342e08e42d68e681fdd12ca66e94611bbc4e70ceabcf38388142c55",
    "zh:d32f97bff69d7c81fdf99dd837fc818940c8cc916ffba91da645ed3f16dc7985",
    "zh:e0eb46c0e41dccf3486b0af14ab9d291f7a5beb768537cf9b828c3fcbba5dd46",
    "zh:f2b00605a955f05bd783e2ed039ec62f05df1c902fe048129cceb660bf5b994a",
  ]
}
