#create VP
resource "tencentcloud_vpc" "vpc01" {
  name         = "${var.name}-01"
  cidr_block   = "${var.vpc_ip_seg}.0.0/16"
  is_multicast = false

  tags = {
    "user" = var.name
  }
}

resource "tencentcloud_subnet" "subset01" {
  count             = length(data.tencentcloud_availability_zones_by_product.all_zones.zones)
  name              = "${var.name}-subset-${count.index}"
  vpc_id            = tencentcloud_vpc.vpc01.id
  availability_zone = data.tencentcloud_availability_zones_by_product.all_zones.zones[count.index].name
  cidr_block        = "${var.vpc_ip_seg}.${count.index}.0/24"
  is_multicast      = false
  tags = {
    "user" = var.name
  }
}

