#providers
terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
    }
  }
}
#list region
provider "tencentcloud" {
  region = var.region
  secret_id  = "my-secret-id"
  secret_key = "my-secret-key"
}
#list az
data "tencentcloud_availability_zones_by_product" "all_zones" {
  product = "test"
}


#create tke_managed cluster
resource "tencentcloud_kubernetes_cluster" "tke_managed" {
  vpc_id                                     = tencentcloud_vpc.vpc01.id
  cluster_version                            = var.k8s_ver
  cluster_cidr                               = "${var.pod_ip_seg}.0.0/16"
  cluster_max_pod_num                        = 64
  cluster_name                               = "${var.name}-tke-01"
  cluster_desc                               = "created by terraform"
  cluster_max_service_num                    = 2048
  cluster_internet                           = true
  managed_cluster_internet_security_policies = ["0.0.0.0/0"]
  cluster_deploy_type                        = "MANAGED_CLUSTER"
  cluster_os                                 = "tlinux2.4x86_64"
  container_runtime                          = "containerd"
  deletion_protection                        = false

  worker_config {
    instance_name              = "${var.name}-node"
    availability_zone          = data.tencentcloud_availability_zones_by_product.all_zones.zones[0].name
    instance_type              = var.default_instance_type
    system_disk_type           = "CLOUD_SSD"
    system_disk_size           = 50
    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 1
    public_ip_assigned         = true
    subnet_id                  = tencentcloud_subnet.subset01[0].id
    security_group_ids         = [tencentcloud_security_group.sg01.id]

    enhanced_security_service = false
    enhanced_monitor_service  = false
    password                  = var.node_password
  }


  labels = {
    "user" = var.name
  }
}

#create node pool
resource "tencentcloud_kubernetes_node_pool" "node-pool" {
  name                 = "${var.name}-pool"
  cluster_id           = tencentcloud_kubernetes_cluster.tke_managed.id
  max_size             = 10
  min_size             = 0
  vpc_id               = tencentcloud_vpc.vpc01.id
  subnet_ids           = [for s in tencentcloud_subnet.subset01 : s.id]
  retry_policy         = "INCREMENTAL_INTERVALS"
  desired_capacity     = 0
  enable_auto_scale    = true
  delete_keep_instance = false
  node_os              = "tlinux2.4x86_64"

  auto_scaling_config {
    instance_type      = var.default_instance_type
    system_disk_type   = "CLOUD_PREMIUM"
    system_disk_size   = "50"
    security_group_ids = [tencentcloud_security_group.sg01.id]

    data_disk {
      disk_type = "CLOUD_PREMIUM"
      disk_size = 50
    }

    internet_charge_type       = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 10
    public_ip_assigned         = true
    password                   = var.node_password
    enhanced_security_service  = false
    enhanced_monitor_service   = false

  }

  labels = {
    "user" = var.name,
  }

}


