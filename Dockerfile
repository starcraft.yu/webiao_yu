#use golang base image
FROM golang:1.16-buster as builder

LABEL maintainer="starcraft.yu@gmail.com"
WORKDIR /app
COPY * ./
ADD ./service-a ./service-a
ADD ./charts ./charts

RUN go mod download

# Copy local code to the container image


ARG GOPROXY=https://goproxy.cn


ENV APP_NAME=service-a
ENV APP_PORT=8888
RUN set -ex; \
    cd /app/service-a; \
    go mod tidy; \
    go build -v -o /servera;




EXPOSE 8888

CMD ["/servera"]

