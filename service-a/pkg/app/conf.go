package app

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config
type Config struct {
	Env        string `yaml:"env"`
	Port       string `yaml:"port"`
	FeatureOne bool   `yaml:"feature_one"`
	FeatureTwo bool   `yaml:"feature_two"`
}

func NewConfig() *Config {
	return &Config{
		Env:  "dev",
		Port: "8080",
	}
}

// LoadFromFile reads configuration from file in yaml format
func (c *Config) LoadFromFile(file string) (err error) {
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(bytes, c)
	if err != nil {
		return fmt.Errorf("parse configuration from file %s failed, err: %s", file, err)
	}
	return
}

func (c *Config) Print() (out []byte, err error) {
	return yaml.Marshal(&c)
}
